var express = require('express');
var router = express.Router();
var db = require('../models/firebase')();

router.get('/', function (req, res, next) {
    db.isLoggedIn(function(data){
        res.render('login/content', {title: 'Login', loggedin: data});
    });
});

router.post('/login', function (req, res, next){
    var loginData = req.body;
    db.login(loginData, function(isLoggedIn){
        res.render('login/content', {title: 'Login', loggedin: isLoggedIn});
    });
});

router.post('/logout', function (req, res, next){
    db.logout(function(data){
        res.render('login/content', {title: 'Logout', loggedin: false, data: data});
    });
});

module.exports = router;
