var express = require('express');
var router = express.Router();
var db = require('../models/firebase')();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Blog'});
});


module.exports = router;
