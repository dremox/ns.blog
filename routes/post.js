var express = require('express');
var router = express.Router();
var db = require('../models/firebase')();
var utils = require('../helpers/utils')();

router.post('/new', function(req, res){
    var postData = req.body;
    db.isLoggedIn(function(isLoggedIn){
        if(isLoggedIn){
            db.post(postData, function(){
                res.redirect('/posts');
            });
        }else{
            res.redirect('/posts');
        }
    });
});

router.get('/delete/:id', function (req, res) {
    db.isLoggedIn(function(isLoggedIn){
        if(isLoggedIn){
            db.deleteSpecific(req.params.id, function(){
                res.redirect('/posts');
            });
        }else{
            res.redirect('/posts');
        }
    });
});

router.get('/', function (req, res) {
    var isLoggedIn = false;
    db.isLoggedIn(function(result){
        isLoggedIn = result;
    });
    db.getAll(function(data){
        res.render('posts/all', {title: 'Posts', isLoggedIn: isLoggedIn, data: data});
    });
});

router.get('/:id', function(req, res){
    var isLoggedIn = false;
    db.isLoggedIn(function(isLoggedIn) {
        db.getSpecific(req.params.id, function(data){
            res.render('posts/individual', {
                title: data.title || '',
                published: utils.formatDate(data.published),
                content: data.content || '',
                id: req.params.id,
                isLoggedIn: isLoggedIn
            });
        });
    });
});

router.post('/:id/edit', function(req, res){
    var postData = req.body;
    var isLoggedIn = false;
    db.isLoggedIn(function(isLoggedIn){
        if(isLoggedIn){
            db.editSpecific(req.params.id, postData, function(){
                res.redirect('/posts/'+req.params.id);
            });
        }else{
            res.redirect('/posts/'+req.params.id);
        }
    });
});

module.exports = router;
