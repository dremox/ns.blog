var Firebase = require('firebase');
var fb = new Firebase("https://ns-blog.firebaseio.com/");

var getAll = function(callBack){
    fb.child('blog').on('value', function(snapshot){
        callBack(snapshot.val());
    });
};


var post = function(data, callBack){
    fb.child('blog').push(
        {
            title: data.title,
            published: Date.now(),
            content: data.content
        }, function(error){
            if (error) {
                callBack(false);
            } else {
                callBack(true);
            }
        }
    )
};

var getSpecific = function(id, callBack){
    fb.child('blog/'+id).on('value', function(snapshot){
        callBack(snapshot.val());
    });
};

var editSpecific = function(id, payload, callBack){
    fb.child('blog/'+id).update(payload, function(error){
        if (error) {
            callBack(false);
        } else {
            callBack(true);
        }
    });
};

var deleteSpecific = function(id, callBack){
    fb.child('blog/'+id).remove(function(error){
        if (error) {
            callBack(false);
        } else {
            callBack(true);
        }
    });
};

var login = function(payload, callBack){
    fb.authWithPassword({
        email    : payload.username,
        password : payload.password
    }, function(error){
        if (error) {
            callBack(false);
        } else {
            callBack(true);
        }
    });
};

var logout = function(callBack){
    callBack(fb.unauth());
};

var isLoggedIn = function(callBack){
    callBack(fb.getAuth());
};

module.exports = function(){
    return {
        login: login,
        logout: logout,
        isLoggedIn: isLoggedIn,
        getSpecific: getSpecific,
        editSpecific: editSpecific,
        deleteSpecific: deleteSpecific,
        getAll: getAll,
        post: post
    }
};