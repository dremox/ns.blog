var formatDate = function(unixTimestamp){
    var date = new Date(unixTimestamp);
    return date.toLocaleDateString();
};

module.exports = function(){
    return {
        formatDate: formatDate
    }
};